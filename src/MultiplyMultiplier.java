public class MultiplyMultiplier implements MultiplicationStrategy {
	@Override
	public int multiply(int a, int b) {
		System.out.printf("Multiplying Strategy: multiplying %d * %d\n", a, b);
		return a * b;
	}
}
