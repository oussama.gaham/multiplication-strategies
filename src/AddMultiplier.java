import static java.lang.Integer.max;
import static java.lang.Integer.min;

public class AddMultiplier implements MultiplicationStrategy {
	@Override
	public int multiply(int a, int b) {
		System.out.printf("Adding Strategy: multiplying %d * %d\n", a, b);
		int res = 0;
		int min = min(a, b);
		int max = max(a, b);
		while(min > 0) {
			res += max;
			min--;
		}
		return res;
	}
}
